﻿namespace SecondMonitor.Rating.Application.Championship.ViewModels.Creation.Calendar
{
    public class MysteryTrackTemplateViewModel : AbstractTrackTemplateViewModel
    {
        public MysteryTrackTemplateViewModel()
        {
            TrackName = "Mystery Track";
        }
    }
}