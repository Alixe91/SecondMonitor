﻿namespace SecondMonitor.Rating.Application.Rating.Controller.SimulatorRating.RatingUpdater
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Common.DataModel;
    using Common.DataModel.Player;
    using DataModel.Summary;

    public interface IRatingUpdater
    {
        Task<(DriversRating newSimulatorRating, DriversRating newClassRating, DriversRating newDifficultyRating)> UpdateRatingsByResults(Dictionary<string, DriversRating> ratings, DriversRating difficultyRating, DriversRating simulatorRating, SessionFinishState sessionFinishState, int difficulty);
        Task<(DriversRating newSimulatorRating, DriversRating newClassRating, DriversRating newDifficultyRating)> UpdateRatingsAsLoss(Dictionary<string, DriversRating> ratingMap, DriversRating difficultyRating, DriversRating simulatorRating, int difficulty, Driver player, string trackName);
    }
}