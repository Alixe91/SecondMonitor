﻿namespace SecondMonitor.Contracts.Attributes
{
    public static class TimingLayoutElementsNames
    {
        public const string MapElement = "Track Map";
        public const string CarSystemsElement = "Car Systems / Wheel Information";
    }
}