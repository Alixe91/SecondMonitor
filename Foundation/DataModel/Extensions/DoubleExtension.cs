﻿namespace SecondMonitor.DataModel.Extensions
{
    using System;

    public static class DoubleExtension
    {
        public static string ToStringScalableDecimals(this double valueD)
        {
            double valueAbs = Math.Abs(valueD);
            return valueD == 0 ? "0" : valueAbs < 0 ?  valueD.ToString("F3") : valueAbs < 10 ? valueD.ToString("F2") : valueAbs < 100 ? valueD.ToString("F1") : valueD.ToString("F0");
        }

        public static string ToStringLimitDecimals(this double valueD)
        {
            return valueD == 0 ? "0" : valueD < 0 ? valueD.ToString("G4") : valueD.ToString("F0");
        }
    }
}