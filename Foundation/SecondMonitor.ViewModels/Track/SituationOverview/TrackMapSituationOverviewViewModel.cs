﻿namespace SecondMonitor.ViewModels.Track.SituationOverview
{
    using System.Collections.Generic;
    using Colors;
    using Contracts.Session;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using DataModel.TrackMap;
    using Factory;
    using Weather;

    public class TrackMapSituationOverviewViewModel : DefaultSituationOverviewViewModel
    {
        private bool _autoScaleDrivers;

        public TrackMapSituationOverviewViewModel(IViewModelFactory viewModelFactory, IClassColorProvider classColorProvider, double layoutLength, bool animateDrivers, int driversUpdatedPerTick) : base(viewModelFactory, classColorProvider, layoutLength, animateDrivers, driversUpdatedPerTick)
        {
            TrackWithSectorsGeometryViewModel = viewModelFactory.Create<TrackWithSectorsGeometryViewModel>();
            WindInformationViewModel = viewModelFactory.Create<WindInformationViewModel>();
        }

        public bool AutoScaleDrivers
        {
            get => _autoScaleDrivers;
            set => SetProperty(ref _autoScaleDrivers, value);
        }

        public TrackWithSectorsGeometryViewModel TrackWithSectorsGeometryViewModel { get; }
        public WindInformationViewModel WindInformationViewModel { get; }

        public override List<DriverInfo> Update(SimulatorDataSet simulatorDataSet, ISessionInformationProvider sessionInformationProvider, bool usePositionInClass)
        {
            WindInformationViewModel.FromModel(simulatorDataSet.SessionInfo.WeatherInfo);
            TrackWithSectorsGeometryViewModel.UpdateSectorStates(simulatorDataSet, sessionInformationProvider);
            return base.Update(simulatorDataSet, sessionInformationProvider, usePositionInClass);
        }

        public void ApplyTrackGeometry(TrackGeometryDto trackGeometryDto)
        {
            WindInformationViewModel.UpdateRotationByGeometryDto(trackGeometryDto);
            TrackWithSectorsGeometryViewModel.FromModel(trackGeometryDto);
        }
    }
}