﻿using System.Windows;

namespace SecondMonitor.ViewModels
{
    using System.Runtime.CompilerServices;

    public abstract class AbstractDependencyViewModel<T> : AbstractDependencyViewModel, IViewModel<T>
    {
        public T OriginalModel { get; private set; }

        protected abstract void ApplyModel(T model);

        public void FromModel(T model)
        {
            OriginalModel = model;
            ApplyModel(model);
        }
        public abstract T SaveToNewModel();
    }

    public abstract class AbstractDependencyViewModel : DependencyObject, IViewModel
    {
        protected void SetProperty<T>(DependencyProperty property, T value, [CallerMemberName]  string propertyName = null)
        {
            T oldValue = (T) GetValue(property);
            if (oldValue!= null && oldValue.Equals(value))
            {
                return;
            }

            SetValue(property, value);
        }
    }


}