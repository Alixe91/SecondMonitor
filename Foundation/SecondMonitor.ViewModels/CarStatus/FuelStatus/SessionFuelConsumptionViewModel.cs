﻿namespace SecondMonitor.ViewModels.CarStatus.FuelStatus
{
    using System;
    using Contracts.FuelInformation;
    using DataModel.BasicProperties;
    using DataModel.Summary;
    using DataModel.Summary.FuelConsumption;

    public class SessionFuelConsumptionViewModel : AbstractViewModel<SessionFuelConsumptionDto>, ISessionFuelConsumptionViewModel
    {
        private string _trackName;
        private Distance _lapDistance;
        private string _sessionType;
        private FuelConsumptionInfo _fuelConsumptionInfo;
        private Volume _avgPerMinute;
        private Volume _avgPerLap;

        public string TrackName
        {
            get => _trackName;
            set => SetProperty(ref _trackName, value);
        }

        public Distance LapDistance
        {
            get => _lapDistance;
            set
            {
                SetProperty(ref _lapDistance, value);
                OnFuelConsumptionChanged();
            }
        }

        public string SessionType
        {
            get => _sessionType;
            set => SetProperty(ref _sessionType, value);
        }

        public FuelConsumptionInfo FuelConsumption
        {
            get => _fuelConsumptionInfo;
            set
            {
                SetProperty(ref _fuelConsumptionInfo, value);
                OnFuelConsumptionChanged();
            }
        }

        public Volume AvgPerMinute
        {
            get => _avgPerMinute;
            set => SetProperty(ref _avgPerMinute, value);
        }

        public Volume AvgPerLap
        {
            get => _avgPerLap;
            set => SetProperty(ref _avgPerLap, value);
        }

        protected override void ApplyModel(SessionFuelConsumptionDto model)
        {
            TrackName = model.TrackFullName;
            LapDistance = Distance.FromMeters(model.LapDistance);
            FuelConsumption = new FuelConsumptionInfo(model.ConsumedFuel, TimeSpan.FromSeconds(model.ElapsedSeconds), Distance.FromMeters(model.TraveledDistanceMeters));
            SessionType = model.IsWetSession ? model.SessionKind + "(WET)" : model.SessionKind.ToString();
        }

        public override SessionFuelConsumptionDto SaveToNewModel()
        {
            throw new System.NotImplementedException();
        }

        private void OnFuelConsumptionChanged()
        {
            if (FuelConsumption == null || LapDistance == null)
            {
                return;
            }

            AvgPerMinute = FuelConsumption.GetAveragePerMinute();
            AvgPerLap = FuelConsumption.GetAveragePerDistance(LapDistance);
        }
    }
}