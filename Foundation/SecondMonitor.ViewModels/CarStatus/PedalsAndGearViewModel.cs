﻿namespace SecondMonitor.ViewModels.CarStatus
{
    using System;
    using System.Diagnostics;
    using System.Windows;
    using DataModel.Snapshot;
    using Settings;

    public class PedalsAndGearViewModel : AbstractDependencyViewModel, ISimulatorDataSetViewModel
    {
        private readonly Stopwatch _refreshStopWatch;
        public const string ViewModelLayoutName = "Pedals And Gear";
        private readonly ISettingsProvider _settingsProvider;
        public static readonly DependencyProperty ThrottlePercentageProperty = DependencyProperty.Register("ThrottlePercentage", typeof(double), typeof(PedalsAndGearViewModel), new PropertyMetadata(default(double)));
        public static readonly DependencyProperty WheelRotationProperty = DependencyProperty.Register("WheelRotation", typeof(double), typeof(PedalsAndGearViewModel), new PropertyMetadata(default(double)));
        public static readonly DependencyProperty ClutchPercentageProperty = DependencyProperty.Register("ClutchPercentage", typeof(double), typeof(PedalsAndGearViewModel), new PropertyMetadata(default(double)));
        public static readonly DependencyProperty BrakePercentageProperty = DependencyProperty.Register("BrakePercentage", typeof(double), typeof(PedalsAndGearViewModel), new PropertyMetadata(default(double)));
        public static readonly DependencyProperty SpeedProperty = DependencyProperty.Register("Speed", typeof(string), typeof(PedalsAndGearViewModel), new PropertyMetadata(default(string)));
        public static readonly DependencyProperty RpmProperty = DependencyProperty.Register("Rpm", typeof(int), typeof(PedalsAndGearViewModel), new PropertyMetadata(default(int)));
        public static readonly DependencyProperty GearProperty = DependencyProperty.Register("Gear", typeof(string), typeof(PedalsAndGearViewModel), new PropertyMetadata(default(string)));

        public PedalsAndGearViewModel(ISettingsProvider settingsProvider)
        {
            _refreshStopWatch = Stopwatch.StartNew();
            _settingsProvider = settingsProvider;
        }

        public double ThrottlePercentage
        {
            get => (double)GetValue(ThrottlePercentageProperty);
            set => SetProperty(ThrottlePercentageProperty, value);
        }

        public string Speed
        {
            get => (string)GetValue(SpeedProperty);
            set => SetProperty(SpeedProperty, value);
        }

        public int Rpm
        {
            get => (int)GetValue(RpmProperty);
            set => SetProperty(RpmProperty, value);
        }

        public double WheelRotation
        {
            get => (double)GetValue(WheelRotationProperty);
            set => SetProperty(WheelRotationProperty, value);
        }

        public double ClutchPercentage
        {
            get => (double)GetValue(ClutchPercentageProperty);
            set => SetProperty(ClutchPercentageProperty, value);
        }

        public double BrakePercentage
        {
            get => (double)GetValue(BrakePercentageProperty);
            set => SetProperty(BrakePercentageProperty, value);
        }

        public double ThrottleFilteredPercentage { get; set; }

        public double BrakeFilteredPercentage { get; set; }

        public string Gear
        {
            get => (string)GetValue(GearProperty);
            set => SetProperty(GearProperty, value);
        }


        public void ApplyDateSet(SimulatorDataSet dataSet)
        {
            if (_refreshStopWatch.ElapsedMilliseconds < 33)
            {
                return;
            }
            if (dataSet?.PlayerInfo?.CarInfo == null || dataSet.InputInfo == null || !_settingsProvider.DisplaySettingsViewModel.EnablePedalInformation)
            {
                return;
            }

            if (!Application.Current.Dispatcher.CheckAccess())
            {
                Application.Current.Dispatcher.Invoke(() => ApplyDateSet(dataSet));
                return;
            }

            if (dataSet.InputInfo.ThrottlePedalPosition >= 0)
            {
                ThrottlePercentage = Math.Round(dataSet.InputInfo.ThrottlePedalPosition * 100);
                //ThrottleFilteredPercentage = dataSet.InputInfo.ThrottlePedalFilteredPosition * 100;
            }

            if (dataSet.InputInfo.BrakePedalPosition >= 0)
            {
                BrakePercentage = Math.Round(dataSet.InputInfo.BrakePedalPosition * 100);
                //BrakeFilteredPercentage = dataSet.InputInfo.BrakePedalFilteredPosition * 100;
            }

            if (dataSet.InputInfo.ClutchPedalPosition >= 0)
            {
                ClutchPercentage = Math.Round(dataSet.InputInfo.ClutchPedalPosition * 100);
            }

            Speed = dataSet.PlayerInfo.Speed.GetValueInUnits(_settingsProvider.DisplaySettingsViewModel.VelocityUnits).ToString("F0");
            Rpm = dataSet.PlayerInfo.CarInfo.EngineRpm;


            WheelRotation = Math.Round(dataSet.InputInfo.WheelAngle);
            Gear = dataSet.PlayerInfo.CarInfo.CurrentGear;

            _refreshStopWatch.Restart();
        }

        public void Reset()
        {

        }
    }
}