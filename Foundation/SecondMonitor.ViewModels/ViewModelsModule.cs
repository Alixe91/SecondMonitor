﻿namespace SecondMonitor.ViewModels
{
    using CarStatus;
    using CarStatus.FuelStatus;
    using Colors;
    using Controllers;
    using Dialogs;
    using Factory;
    using FuelConsumption;
    using Layouts;
    using Layouts.Editor;
    using Layouts.Factory;
    using Ninject.Modules;
    using PluginsSettings;
    using SessionEvents;
    using Settings;
    using Settings.ViewModel;
    using SimulatorContent;
    using SplashScreen;
    using Track;
    using Track.SituationOverview;
    using Track.SituationOverview.Controller;
    using TrackRecords;
    using Weather;
    using WheelDiameterWizard;

    public class ViewModelsModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IViewModelFactory>().To<ViewModelFactory>();
            Bind<IChildControllerFactory>().To<ChildControllerFactory>();
            Bind<IWindowService>().To<WindowService>();
            Bind<IDialogService>().To<DialogService>();
            Bind<IBroadcastLimitSettingsViewModel>().To<BroadcastLimitSettingsViewModel>();
            Bind<IPluginConfigurationViewModel>().To<PluginConfigurationViewModel>();
            Bind<IPluginsConfigurationViewModel>().To<PluginsConfigurationViewModel>();
            Bind<IRemoteConfigurationViewModel>().To<RemoteConfigurationViewModel>();
            Bind<F12019ConfigurationViewModel>().ToSelf();
            Bind<PCars2ConfigurationViewModel>().ToSelf();
            Bind<AccConfigurationViewModel>().ToSelf();
            Bind<ISettingsProvider>().To<AppDataSettingsProvider>().InSingletonScope();
            Bind<ISimulatorContentRepository>().To<StoredSimulatorContentRepository>().InSingletonScope();
            Bind<ISimulatorContentController, ISimulatorContentProvider>().To<SimulatorContentController>().InSingletonScope();

            Bind<ITrackRecordsViewModel>().To<TrackRecordsViewModel>();
            Bind<IRecordViewModel>().To<RecordViewModel>();

            Bind<DisplaySettingsViewModel>().ToSelf();

            Bind<TrackRecordViewModel>().ToSelf();
            Bind<CarRecordViewModel>().ToSelf();
            Bind<SimulatorRecordsViewModel>().ToSelf();
            Bind<CarRecordsCollectionViewModel>().ToSelf();
            Bind<RecordEntryViewModel>().ToSelf();
            Bind<WelcomeStageViewModel>().ToSelf();
            Bind<AccelerationStageViewModel>().ToSelf();
            Bind<PreparationStageViewModel>().ToSelf();
            Bind<MeasurementPhaseViewModel>().ToSelf();
            Bind<ResultsStageViewModel>().ToSelf();
            Bind<SplashScreenViewModel>().ToSelf();
            Bind<TrackGeometryViewModel>().ToSelf();
            Bind<TrackWithSectorsGeometryViewModel>().ToSelf();

            Bind<WheelStatusViewModel>().ToSelf();
            Bind<WheelStatusViewModelFactory>().ToSelf();

            Bind<ISessionEventProvider>().To<SessionEventProvider>().InSingletonScope();

            Bind<YesNoDialogViewModel>().ToSelf();
            Bind<SituationOverviewViewModelFactory>().ToSelf();
            Bind<SituationOverviewController>().ToSelf().InSingletonScope();
            Bind<IMapSidePanelViewModel>().To<MapSidePanelViewModel>();
            Bind<IClassColorProvider>().To<ClassColorProvider>().InSingletonScope();
            Bind<IColorPaletteProvider>().To<BasicColorPaletteProvider>().WhenInjectedExactlyInto<ClassColorProvider>();
            Bind<FuelConsumptionRepository>().ToSelf().InSingletonScope();
            Bind<FuelConsumptionController, IFuelPredictionProvider>().To<FuelConsumptionController>().InSingletonScope();
            Bind<FuelPlannerViewModelFactory>().ToSelf();
            Bind<CarStatusViewModel>().ToSelf();
            Bind<WindInformationViewModel>().ToSelf();
            Bind<ILayoutFactory>().To<LayoutFactory>();

            Bind<RowsDefinitionSettingViewModel>().ToSelf();
            Bind<ILayoutEditorManipulator>().To<LayoutEditorManipulator>();
            Bind<ILayoutConfigurationViewModelFactory>().To<LayoutConfigurationViewModelFactory>();
        }
    }
}
