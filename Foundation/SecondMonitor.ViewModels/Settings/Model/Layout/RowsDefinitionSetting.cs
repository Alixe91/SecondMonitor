﻿namespace SecondMonitor.ViewModels.Settings.Model.Layout
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class RowsDefinitionSetting : GenericContentSetting
    {
        public RowsDefinitionSetting()
        {
        }

        public RowsDefinitionSetting(int rowCount, LengthDefinitionSetting[] rowsSize, GenericContentSetting[] rowsContent, bool addGridSplitters)
        {
            RowCount = rowCount;
            RowsSize = rowsSize;
            RowsContent = rowsContent;
            AddGridSplitters = addGridSplitters;
        }

        public bool AddGridSplitters { get; set; }
        public int RowCount { get; set; }
        public LengthDefinitionSetting[] RowsSize { get; set; }
        public GenericContentSetting[] RowsContent { get; set; }
    }
}