﻿namespace SecondMonitor.AccConnector
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using DataModel;
    using DataModel.BasicProperties;
    using DataModel.Snapshot;
    using DataModel.Snapshot.Drivers;
    using ksBroadcastingNetwork;
    using ksBroadcastingNetwork.Structs;
    using NLog;
    using SharedMemory;
    using UDPData;
    using CarInfo = DataModel.Snapshot.Systems.CarInfo;
    using DriverInfo = DataModel.Snapshot.Drivers.DriverInfo;
    using SessionPhase = DataModel.Snapshot.SessionPhase;

    public class AccDataConverter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly string _customPlayerName;
        private const double MaxWear = 91;
        private readonly Dictionary<string, TimeSpan[]> _currentSectorTimes;
        private readonly Stopwatch _sessionTimeWatch;
        private SPageFilePhysics _lastValidPhysics;
        private int? _sectorLength;
        private SimulatorDataSet _lastValidDataSet;

        private DriverInfo _lastPlayer;

        private readonly Stopwatch _brakePadsStopwatch;



        public AccDataConverter(string customPlayerName)
        {
            _customPlayerName = customPlayerName;
            _sessionTimeWatch = Stopwatch.StartNew();
            _currentSectorTimes = new Dictionary<string, TimeSpan[]>();
            _brakePadsStopwatch = Stopwatch.StartNew();
        }

        private static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public void Reset()
        {
            _lastValidDataSet = null;
            _currentSectorTimes.Clear();
            _sectorLength = null;
            _sessionTimeWatch.Start();
        }

        public SimulatorDataSet CreateSimulatorDataSet(AccShared accData, FullUdpData fullUdpData)
        {
            SimulatorDataSet simData = new SimulatorDataSet(SimulatorsNameMap.ACCSimName)
            {
                SimulatorSourceInfo =
                {
                    HasLapTimeInformation = true,
                    OutLapIsValid = false,
                    SimNotReportingEndOfOutLapCorrectly = true,
                    ForceLapOverTime = true,
                    GlobalTyreCompounds = false,
                    SectorTimingSupport = DataInputSupport.Full,
                    TelemetryInfo = {ContainsSuspensionTravel = false, ContainsWheelRps = true}
                }
            };

            if (_lastValidPhysics.waterTemp > 0 && accData.AcsPhysics.waterTemp <= 0)
            {
                accData.AcsPhysics = _lastValidPhysics;
            }
            else
            {
                _lastValidPhysics = accData.AcsPhysics;
            }

            if ((!fullUdpData.CarsDictionary.TryGetValue(accData.AcsGraphic.playerCarID, out FullCarData dummy)) && _lastValidDataSet != null )
            {
                return _lastValidDataSet;
            }


            FillSessionInfo(accData, simData, fullUdpData);
            FillPitWindowInformation(accData, simData, fullUdpData);
            AddDriversData(simData, accData, fullUdpData);

            FillPlayerCarInfo(accData, simData);

            // PEDAL INFO
            AddPedalInfo(accData, simData);

            // WaterSystemInfo
            AddWaterSystemInfo(simData, accData);

            // OilSystemInfo
            AddOilSystemInfo(simData, accData);

            // Brakes Info
            AddBrakesInfo(accData, simData);

            // Tyre Pressure Info
            AddTyresAndFuelInfo(simData, accData);

             // Acceleration
            AddAcceleration(simData, accData);

            //Add Additional Player Car Info
            AddPlayerCarInfo(accData, simData);

            _lastValidDataSet = simData;
            return simData;
        }

        private void AddPlayerCarInfo(AccShared data, SimulatorDataSet simData)
        {
            CarInfo playerCar = simData.PlayerInfo.CarInfo;
            playerCar.CarDamageInformation.Bodywork.MediumDamageThreshold = 0.1;
            playerCar.CarDamageInformation.Bodywork.HeavyDamageThreshold = 0.9;
            playerCar.CarDamageInformation.Bodywork.Damage = data.AcsPhysics.carDamage.Max() / 100.0;
            simData.PlayerInfo.CarInfo.SpeedLimiterEngaged = data.AcsPhysics.pitLimiterOn == 1;


            playerCar.WorldOrientation = new Orientation()
            {
                Pitch = Angle.GetFromRadians(data.AcsPhysics.pitch),
                Yaw = Angle.GetFromRadians(data.AcsPhysics.heading),
                Roll = Angle.GetFromRadians(data.AcsPhysics.roll),
            };
        }

        private void FillPlayerCarInfo(AccShared accData, SimulatorDataSet simData)
        {
            FillPlayersGear(accData, simData);
            simData.PlayerInfo.CarInfo.EngineRpm = accData.AcsPhysics.rpms;
        }

        private static void FillPlayersGear(AccShared accData, SimulatorDataSet simData)
        {
            int gear = accData.AcsPhysics.gear - 1;
            switch (gear)
            {
                case 0:
                    simData.PlayerInfo.CarInfo.CurrentGear = "N";
                    break;
                case -1:
                    simData.PlayerInfo.CarInfo.CurrentGear = "R";
                    break;
                case -2:
                    simData.PlayerInfo.CarInfo.CurrentGear = string.Empty;
                    break;
                default:
                    simData.PlayerInfo.CarInfo.CurrentGear = gear.ToString();
                    break;
            }
        }

        private static void AddAcceleration(SimulatorDataSet simData, AccShared accData)
        {
            simData.PlayerInfo.CarInfo.Acceleration.XinMs = accData.AcsPhysics.accG[0] * 9.8;
            simData.PlayerInfo.CarInfo.Acceleration.YinMs = accData.AcsPhysics.accG[1] * 9.8;
            simData.PlayerInfo.CarInfo.Acceleration.ZinMs = accData.AcsPhysics.accG[2] * 9.8;
        }

        private static void AddBrakesInfo(AccShared accData, SimulatorDataSet simData)
        {
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.BrakeTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.brakeTemp[(int)AcWheels.FL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.BrakeTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.brakeTemp[(int)AcWheels.FR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.BrakeTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.brakeTemp[(int)AcWheels.RL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.BrakeTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.brakeTemp[(int)AcWheels.RR]);
        }

        private static void AddOilSystemInfo(SimulatorDataSet simData, AccShared accData)
        {
            simData.PlayerInfo.CarInfo.TurboPressure = Pressure.FromAtm(accData.AcsPhysics.turboBoost);
            simData.PlayerInfo.CarInfo.WaterSystemInfo.OptimalWaterTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.waterTemp);
        }

        private static void AddWaterSystemInfo(SimulatorDataSet simData, AccShared data)
        {
            simData.PlayerInfo.CarInfo.OilSystemInfo.OptimalOilTemperature.ActualQuantity = Temperature.FromCelsius(data.AcsPhysics.waterTemp);
        }

        internal void FillSessionInfo(AccShared data, SimulatorDataSet simData, FullUdpData fullUdpData)
        {
            // Timing
            //simData.SessionInfo.SessionTime = fullUdpData.LastRealtimeUpdate.SessionTime < TimeSpan.Zero ? _sessionTimeWatch.Elapsed : fullUdpData.LastRealtimeUpdate.SessionTime;
            simData.SessionInfo.SessionTime = _sessionTimeWatch.Elapsed;
            simData.SessionInfo.TrackInfo.LayoutLength = Distance.FromMeters(fullUdpData.CurrentTrack.TrackMeters);
            simData.SessionInfo.TrackInfo.TrackName = fullUdpData.CurrentTrack.TrackName;
            simData.SessionInfo.TrackInfo.TrackLayoutName = string.Empty;
            simData.SessionInfo.WeatherInfo.AirTemperature = Temperature.FromCelsius(fullUdpData.LastRealtimeUpdate.AmbientTemp);
            simData.SessionInfo.WeatherInfo.TrackTemperature = Temperature.FromCelsius(fullUdpData.LastRealtimeUpdate.TrackTemp);
            simData.SessionInfo.WeatherInfo.RainIntensity = (int)(fullUdpData.LastRealtimeUpdate.RainLevel * 100);
            simData.SessionInfo.WeatherInfo.TrackWetness = (int)(fullUdpData.LastRealtimeUpdate.Wetness * 100);
            simData.SessionInfo.WeatherInfo.HasWindInformation = true;
            simData.SessionInfo.WeatherInfo.WindSpeed = Velocity.FromKph(data.AcsGraphic.windSpeed);
            simData.SessionInfo.WeatherInfo.WindDirectionFrom = data.AcsGraphic.windDirection;
            //simData.SessionInfo.WeatherInfo.WindDirectionFrom = (data.AcsGraphic.windDirection + 180) % 360;
            simData.SessionInfo.IsMultiplayer = data.AcsStatic.isOnline == 1;
            simData.SessionInfo.IsMultiClass = false;

            switch (fullUdpData.LastRealtimeUpdate.SessionType)
            {
                case RaceSessionType.Practice:
                    simData.SessionInfo.SessionType = SessionType.Practice;
                    break;
                case RaceSessionType.Qualifying:
                    simData.SessionInfo.SessionType = SessionType.Qualification;
                    break;
                case RaceSessionType.Superpole:
                    simData.SessionInfo.SessionType = SessionType.Qualification;
                    break;
                case RaceSessionType.Race:
                    simData.SessionInfo.SessionType = SessionType.Race;
                    break;
                case RaceSessionType.Hotlap:
                    simData.SessionInfo.SessionType = SessionType.Practice;
                    break;
                case RaceSessionType.Hotstint:
                    simData.SessionInfo.SessionType = SessionType.Practice;
                    break;
                case RaceSessionType.HotlapSuperpole:
                    simData.SessionInfo.SessionType = SessionType.Qualification;
                    break;
                case RaceSessionType.Replay:
                default:
                    simData.SessionInfo.SessionType = SessionType.Na;
                    break;
            }

            switch (fullUdpData.LastRealtimeUpdate.Phase)
            {
                case ksBroadcastingNetwork.SessionPhase.NONE:
                    simData.SessionInfo.SessionPhase = SessionPhase.Unavailable;
                    break;
                case ksBroadcastingNetwork.SessionPhase.Starting:
                case ksBroadcastingNetwork.SessionPhase.PreFormation:
                case ksBroadcastingNetwork.SessionPhase.FormationLap:
                case ksBroadcastingNetwork.SessionPhase.PreSession:
                    simData.SessionInfo.SessionPhase = SessionPhase.Countdown;
                    break;
                case ksBroadcastingNetwork.SessionPhase.Session:
                    simData.SessionInfo.SessionPhase = SessionPhase.Green;
                    break;
                case ksBroadcastingNetwork.SessionPhase.SessionOver:
                case ksBroadcastingNetwork.SessionPhase.PostSession:
                case ksBroadcastingNetwork.SessionPhase.ResultUI:
                    simData.SessionInfo.SessionPhase = SessionPhase.Checkered;
                    break;
                default:
                    simData.SessionInfo.SessionPhase = SessionPhase.Unavailable;
                    break;

            }

            simData.SessionInfo.IsActive = data.AcsGraphic.status == AcStatus.AcLive;

            if (simData.SessionInfo.IsActive && !_sessionTimeWatch.IsRunning)
            {
                _sessionTimeWatch.Start();
            }

            if (!simData.SessionInfo.IsActive && _sessionTimeWatch.IsRunning)
            {
                _sessionTimeWatch.Stop();
            }

            simData.SessionInfo.SessionLengthType = SessionLengthType.Time;
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (simData.SessionInfo.SessionType != SessionType.Race && fullUdpData.LastRealtimeUpdate.SessionEndTime.TotalSeconds <= 0)
            {
                simData.SessionInfo.SessionTimeRemaining = 120000;
            }
            else
            {
                simData.SessionInfo.SessionTimeRemaining = fullUdpData.LastRealtimeUpdate.SessionEndTime.TotalSeconds;
            }

            _sectorLength = (int)simData.SessionInfo.TrackInfo.LayoutLength.InMeters / 3;

        }

        private void FillPitWindowInformation(AccShared data, SimulatorDataSet simData, FullUdpData fullUdpData)
        {
            if (data.AcsStatic.PitWindowStart <= 0 || data.AcsStatic.PitWindowStart == data.AcsStatic.PitWindowEnd || simData.SessionInfo.SessionType != SessionType.Race)
            {
                return;
            }
            TimeSpan timeDifference = simData.SessionInfo.SessionTime - fullUdpData.LastRealtimeUpdate.SessionTime;
            TimeSpan totalHalfSessionTime = TimeSpan.FromMilliseconds((fullUdpData.LastRealtimeUpdate.SessionTime + fullUdpData.LastRealtimeUpdate.SessionEndTime).TotalMilliseconds / 2.0);
            TimeSpan pitWindowHalfDuration = TimeSpan.FromMilliseconds((data.AcsStatic.PitWindowEnd - data.AcsStatic.PitWindowStart) / 2.0);
            TimeSpan pitWindowStart = totalHalfSessionTime - pitWindowHalfDuration;
            TimeSpan pitWindowEnd = totalHalfSessionTime + pitWindowHalfDuration;
            simData.SessionInfo.PitWindow.PitWindowStart = (timeDifference + pitWindowStart).TotalMinutes;
            simData.SessionInfo.PitWindow.PitWindowEnd = (timeDifference + pitWindowEnd).TotalMinutes;

            if (pitWindowStart > fullUdpData.LastRealtimeUpdate.SessionTime)
            {
                simData.SessionInfo.PitWindow.PitWindowState = PitWindowState.BeforePitWindow;
            }
            else if(pitWindowEnd > fullUdpData.LastRealtimeUpdate.SessionTime)
            {
                simData.SessionInfo.PitWindow.PitWindowState = PitWindowState.InPitWindow;
            }
            else
            {
                simData.SessionInfo.PitWindow.PitWindowState = PitWindowState.AfterPitWindow;
            }

        }

        private static void AddPedalInfo(AccShared accData, SimulatorDataSet simData)
        {
            simData.InputInfo.ThrottlePedalPosition = accData.AcsPhysics.gas;
            simData.InputInfo.BrakePedalPosition = accData.AcsPhysics.brake;
            simData.InputInfo.ClutchPedalPosition = 1 - accData.AcsPhysics.clutch;
            simData.InputInfo.SteeringInput = accData.AcsPhysics.steerAngle;
        }


        private void AddTyresAndFuelInfo(SimulatorDataSet simData, AccShared accData)
        {
            if (_brakePadsStopwatch.Elapsed.Minutes > 5)
            {
                Logger.Info($"Brake Pads:  {accData.AcsPhysics.padLife[(int)AcWheels.FL]:N1}, Disc Life : {accData.AcsPhysics.discLifeLife[(int)AcWheels.FL]:N1}");
                _brakePadsStopwatch.Restart();
            }

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyreWear.ActualWear = 0;
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyreWear.ActualWear = 0;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyreWear.ActualWear = 0;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyreWear.ActualWear = 0;

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.Camber = Angle.GetFromRadians(accData.AcsPhysics.camberRAD[(int) AcWheels.FL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.Camber = Angle.GetFromRadians(-accData.AcsPhysics.camberRAD[(int) AcWheels.FR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.Camber = Angle.GetFromRadians(accData.AcsPhysics.camberRAD[(int) AcWheels.RL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.Camber = Angle.GetFromRadians(-accData.AcsPhysics.camberRAD[(int) AcWheels.RR]);

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.Rps = accData.AcsPhysics.wheelAngularSpeed[(int) AcWheels.FL];
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.Rps = accData.AcsPhysics.wheelAngularSpeed[(int) AcWheels.FR];
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.Rps = accData.AcsPhysics.wheelAngularSpeed[(int) AcWheels.RL];
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.Rps = accData.AcsPhysics.wheelAngularSpeed[(int) AcWheels.RR];

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.DirtLevel = accData.AcsPhysics.tyreDirtyLevel[(int) AcWheels.FL];
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.DirtLevel = accData.AcsPhysics.tyreDirtyLevel[(int) AcWheels.FR];
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.DirtLevel = accData.AcsPhysics.tyreDirtyLevel[(int) AcWheels.RL];
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.DirtLevel = accData.AcsPhysics.tyreDirtyLevel[(int) AcWheels.RR];

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyreLoad = Force.GetFromNewtons(accData.AcsPhysics.wheelLoad[(int) AcWheels.FL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyreLoad = Force.GetFromNewtons(accData.AcsPhysics.wheelLoad[(int) AcWheels.FR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyreLoad = Force.GetFromNewtons(accData.AcsPhysics.wheelLoad[(int) AcWheels.RL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyreLoad = Force.GetFromNewtons(accData.AcsPhysics.wheelLoad[(int) AcWheels.RR]);

            simData.PlayerInfo.CarInfo.FrontHeight = Distance.FromMeters(accData.AcsPhysics.rideHeight[0]);
            simData.PlayerInfo.CarInfo.RearHeight = Distance.FromMeters(accData.AcsPhysics.rideHeight[1]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.RideHeight = simData.PlayerInfo.CarInfo.FrontHeight;
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.RideHeight = simData.PlayerInfo.CarInfo.FrontHeight;

            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.RideHeight = simData.PlayerInfo.CarInfo.RearHeight;
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.RideHeight = simData.PlayerInfo.CarInfo.RearHeight;

            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.SuspensionTravel = Distance.FromMeters(accData.AcsPhysics.suspensionTravel[(int) AcWheels.FL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.SuspensionTravel = Distance.FromMeters(accData.AcsPhysics.suspensionTravel[(int) AcWheels.FR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.SuspensionTravel = Distance.FromMeters(accData.AcsPhysics.suspensionTravel[(int) AcWheels.RL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.SuspensionTravel = Distance.FromMeters(accData.AcsPhysics.suspensionTravel[(int) AcWheels.RR]);

            // Front Left Tyre Temps
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.LeftTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.FL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.RightTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.FL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.CenterTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.FL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyreCoreTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.FL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyreType = accData.AcsGraphic.tyreCompound;

            // Front Right Tyre Temps
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.LeftTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.FR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.RightTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.FR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.CenterTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.FR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyreCoreTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.FR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyreType = accData.AcsGraphic.tyreCompound;


            // Rear Left Tyre Temps
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.LeftTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.RL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.RightTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.RL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.CenterTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.RL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyreCoreTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.RL]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyreType = accData.AcsGraphic.tyreCompound;

            // Rear Right Tyre Temps
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.LeftTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.RR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.RightTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.RR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.CenterTyreTemp.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.RR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyreCoreTemperature.ActualQuantity = Temperature.FromCelsius(accData.AcsPhysics.tyreCoreTemperature[(int) AcWheels.RR]);
            simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyreType = accData.AcsGraphic.tyreCompound;

            if (simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyreType == "dry_compound")
            {
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyrePressure.ActualQuantity = Pressure.FromPsi(accData.AcsPhysics.wheelsPressure[(int) AcWheels.FL]);
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyrePressure.IdealQuantity = Pressure.FromKiloPascals(193);
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyrePressure.IdealQuantityWindow = Pressure.FromKiloPascals(7);

                simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyrePressure.ActualQuantity = Pressure.FromPsi(accData.AcsPhysics.wheelsPressure[(int) AcWheels.FR]);
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyrePressure.IdealQuantity = Pressure.FromKiloPascals(193);
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyrePressure.IdealQuantityWindow = Pressure.FromKiloPascals(7);

                simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyrePressure.ActualQuantity = Pressure.FromPsi(accData.AcsPhysics.wheelsPressure[(int) AcWheels.RL]);
                simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyrePressure.IdealQuantity = Pressure.FromKiloPascals(193);
                simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyrePressure.IdealQuantityWindow = Pressure.FromKiloPascals(7);

                simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyrePressure.ActualQuantity = Pressure.FromPsi(accData.AcsPhysics.wheelsPressure[(int) AcWheels.RR]);
                simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyrePressure.IdealQuantity = Pressure.FromKiloPascals(193);
                simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyrePressure.IdealQuantityWindow = Pressure.FromKiloPascals(7);
            }
            else
            {
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyrePressure.ActualQuantity = Pressure.FromPsi(accData.AcsPhysics.wheelsPressure[(int)AcWheels.FL]);
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyrePressure.IdealQuantity = Pressure.FromKiloPascals(205);
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontLeft.TyrePressure.IdealQuantityWindow = Pressure.FromKiloPascals(5);

                simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyrePressure.ActualQuantity = Pressure.FromPsi(accData.AcsPhysics.wheelsPressure[(int)AcWheels.FR]);
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyrePressure.IdealQuantity = Pressure.FromKiloPascals(205);
                simData.PlayerInfo.CarInfo.WheelsInfo.FrontRight.TyrePressure.IdealQuantityWindow = Pressure.FromKiloPascals(5);

                simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyrePressure.ActualQuantity = Pressure.FromPsi(accData.AcsPhysics.wheelsPressure[(int)AcWheels.RL]);
                simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyrePressure.IdealQuantity = Pressure.FromKiloPascals(205);
                simData.PlayerInfo.CarInfo.WheelsInfo.RearLeft.TyrePressure.IdealQuantityWindow = Pressure.FromKiloPascals(5);

                simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyrePressure.ActualQuantity = Pressure.FromPsi(accData.AcsPhysics.wheelsPressure[(int)AcWheels.RR]);
                simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyrePressure.IdealQuantity = Pressure.FromKiloPascals(205);
                simData.PlayerInfo.CarInfo.WheelsInfo.RearRight.TyrePressure.IdealQuantityWindow = Pressure.FromKiloPascals(5);
            }

            // Fuel System
            simData.PlayerInfo.CarInfo.FuelSystemInfo.FuelCapacity = Volume.FromLiters(accData.AcsStatic.maxFuel);
            simData.PlayerInfo.CarInfo.FuelSystemInfo.FuelRemaining = Volume.FromLiters(accData.AcsPhysics.fuel);
        }





        internal void AddDriversData(SimulatorDataSet data, AccShared accData, FullUdpData udpData)
        {
            if (accData.AcsGraphic.carCount < 1)
            {
                return;
            }

            List<DriverInfo> driverInfos = new List<DriverInfo>();
            DriverInfo playersInfo = null;

            for (int i = 0; i < accData.AcsGraphic.carCount; i++)
            {
                int carId = accData.AcsGraphic.carIDs[i];
                if (!udpData.CarsDictionary.TryGetValue(carId, out FullCarData carData))
                {
                    continue;
                }

                DriverInfo driverInfo = CreateDriverInfo(accData, carData, udpData.CurrentTrack, i, data);

                if (data.SessionInfo.SessionType == SessionType.Race)
                {
                    driverInfo.CurrentLapValid = !carData.LastUpdate.CurrentLap.IsInvalid;
                }
                else
                {
                    driverInfo.CurrentLapValid = carData.LastUpdate.CurrentLap.Type == LapType.Regular && carData.LastUpdate.CurrentLap.IsValidForBest && !carData.LastUpdate.CurrentLap.IsInvalid;
                }

                if (driverInfo.IsPlayer)
                {
                    playersInfo = driverInfo;
                }

                driverInfos.Add(driverInfo);
                if (driverInfo.Position == 1)
                {
                    data.SessionInfo.LeaderCurrentLap = driverInfo.CompletedLaps + 1;
                    data.LeaderInfo = driverInfo;
                }

                AddLappingInformation(data, udpData.CurrentTrack, driverInfo);
                FillTimingInfo(driverInfo, carData, data);


            }
            CheckValidityByPlayer(playersInfo);
            _lastPlayer = playersInfo;
            if (playersInfo != null)
            {
                data.PlayerInfo = playersInfo;
            }

            data.DriversInfo = driverInfos.ToArray();
        }


        private void CheckValidityByPlayer(DriverInfo driver)
        {
            if (_lastPlayer == null || driver == null || (!_lastPlayer.InPits && driver.InPits))
            {
                return;
            }

        }

        internal void FillTimingInfo(DriverInfo driverInfo, FullCarData carData, SimulatorDataSet dataSet)
        {
            driverInfo.Timing.LastLapTime = CreateTimeSpan(carData.LastUpdate.LastLap.LaptimeMS ?? 0);
            driverInfo.Timing.CurrentLapTime = CreateTimeSpan(carData.LastUpdate.CurrentLap.LaptimeMS ?? 0);
            driverInfo.Timing.CurrentSector = -1;

            if (_sectorLength == null || (dataSet.SessionInfo.SessionType == SessionType.Race && driverInfo.CompletedLaps == 0))
            {
                return;
            }

            int currentSector = (int)driverInfo.LapDistance / _sectorLength.Value;
            TimeSpan[] splits = GetCurrentSplitTimes(driverInfo.DriverSessionId);
            if (splits == null || splits.Length <= currentSector)
            {
                return;
            }

            splits[currentSector] = driverInfo.Timing.CurrentLapTime;
            driverInfo.Timing.CurrentSector = currentSector + 1;
            driverInfo.Timing.LastSector1Time = splits[0];
            driverInfo.Timing.LastSector2Time = splits[1] - splits[0];
            driverInfo.Timing.LastSector3Time = splits[2] - splits[1];
            driverInfo.Timing.CurrentSectorTime = splits[currentSector];

        }

        private TimeSpan[] GetCurrentSplitTimes(string driverId)
        {
            return GetSplitTimes(driverId, _currentSectorTimes);
        }

        private TimeSpan[] GetSplitTimes(string driverId, Dictionary<string, TimeSpan[]> splitsDictionary)
        {
            if (!splitsDictionary.ContainsKey(driverId))
            {
                splitsDictionary[driverId] = new[] { TimeSpan.Zero, TimeSpan.Zero, TimeSpan.Zero };
            }

            return splitsDictionary[driverId];
        }

        private TimeSpan CreateTimeSpan(double miliseconds)
        {
            return miliseconds > 0 ? TimeSpan.FromMilliseconds(miliseconds) : TimeSpan.Zero;
        }

        private void AddLappingInformation(SimulatorDataSet data, TrackData trackData, DriverInfo driverInfo)
        {
            if (data.SessionInfo.SessionType == SessionType.Race && _lastPlayer != null
                && _lastPlayer.CompletedLaps != 0)
            {
                driverInfo.IsBeingLappedByPlayer =
                    driverInfo.TotalDistance < (_lastPlayer.TotalDistance - trackData.TrackMeters * 0.5);
                driverInfo.IsLappingPlayer =
                    _lastPlayer.TotalDistance < (driverInfo.TotalDistance - trackData.TrackMeters * 0.5);
            }
        }

        private DriverInfo CreateDriverInfo(AccShared accData, FullCarData carData, TrackData trackData, int carIndex, SimulatorDataSet dataSet)
        {
            DriverInfo driverInfo = new DriverInfo
            {
                DriverSessionId = carData.LongName,
                CompletedLaps = carData.LastUpdate.Laps,
                CarName = carData.CarName,
                InPits = carData.LastUpdate.CarLocation == CarLocationEnum.Pitlane || carData.LastUpdate.CarLocation == CarLocationEnum.NONE,
                IsPlayer = carData.CarInfo.CarIndex == accData.AcsGraphic.playerCarID,
                Position = carData.LastUpdate.Position,
                Speed = Velocity.FromKph(carData.LastUpdate.Kmh),
                LapDistance = trackData.TrackMeters * carData.LastUpdate.SplinePosition,
                FinishStatus = FromAcStatus(dataSet.SessionInfo.SessionPhase, carData),
                WorldPosition = new Point3D(Distance.FromMeters(accData.AcsGraphic.carCoordinates[carIndex].x), Distance.FromMeters(accData.AcsGraphic.carCoordinates[carIndex].y), Distance.FromMeters(accData.AcsGraphic.carCoordinates[carIndex].z)),
                CarClassName = carData.ClassName,
                PositionInClass = carData.LastUpdate.Position,
                DriverShortName = carData.ShortName,
                DriverLongName = carData.LongName,
                CarClassId = "ACC-"+ carData.ClassName,
            };

            if (driverInfo.IsPlayer && !string.IsNullOrWhiteSpace(_customPlayerName))
            {
                driverInfo.DriverSessionId = _customPlayerName;
                driverInfo.DriverLongName = _customPlayerName;
                driverInfo.DriverShortName = _customPlayerName;
            }

            driverInfo.TotalDistance = driverInfo.CompletedLaps * trackData.TrackMeters + driverInfo.LapDistance;

            ComputeDistanceToPlayer(_lastPlayer, driverInfo, trackData);
            return driverInfo;
        }

        internal static void ComputeDistanceToPlayer(DriverInfo player, DriverInfo driverInfo, TrackData trackData)
        {
            if (player == null)
            {
                return;
            }

            if (driverInfo.FinishStatus == DriverFinishStatus.Dq || driverInfo.FinishStatus == DriverFinishStatus.Dnf ||
                driverInfo.FinishStatus == DriverFinishStatus.Dnq || driverInfo.FinishStatus == DriverFinishStatus.Dns)
            {
                driverInfo.DistanceToPlayer = double.MaxValue;
                return;
            }

            double trackLength = trackData.TrackMeters;
            double playerLapDistance = player.LapDistance;

            double distanceToPlayer = playerLapDistance - driverInfo.LapDistance;
            if (distanceToPlayer < -(trackLength / 2))
            {
                distanceToPlayer += trackLength;
            }

            if (distanceToPlayer > (trackLength / 2))
            {
                distanceToPlayer -= trackLength;
            }

            driverInfo.DistanceToPlayer = distanceToPlayer;
        }


        internal static DriverFinishStatus FromAcStatus(SessionPhase sessionPhase, FullCarData carData)
        {
            if (carData.IsFinished && sessionPhase == SessionPhase.Checkered)
            {
                return DriverFinishStatus.Finished;
            }
            if (carData.LastUpdate.CarLocation == CarLocationEnum.NONE)
            {
                return DriverFinishStatus.Dnf;
            }

            return DriverFinishStatus.None;
        }

        private static string FormatACName(string name)
        {
            return name.Replace("ks_", string.Empty).Replace("_", " ");
        }
    }
}