﻿namespace SecondMonitor.R3EConnector
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.IO.MemoryMappedFiles;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Threading;
    using System.Threading.Tasks;
    using DataModel;
    using DataModel.Snapshot;
    using NLog;
    using PluginManager.GameConnector;
    using DriverInfo = DataModel.Snapshot.Drivers.DriverInfo;

    public class R3EConnector : AbstractGameConnector
    {
        private readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly string[] R3EExecutables = { "RRRE", "RRRE64" };
        private static readonly string SharedMemoryName = "$R3E";
        private MemoryMappedFile _sharedMemory;
        private bool _inSession;
        private int _lastSessionType;
        private int _lastSessionPhase;
        private Dictionary<string, DriverInfo> _lastTickInformation;
        private DateTime _lastPlayerMultipleTimesLogged;

        private TimeSpan _sessionTime;
        private TimeSpan _lastSessionTime = TimeSpan.Zero;

        private DateTime _startSessionTime;
        private double _sessionStartR3RTime;

       private R3EDataConverter DataConverter { get; set; }

        public R3EConnector() : base(R3EExecutables)
        {
            DataConverter = new R3EDataConverter(this);
            TickTime = 16;
        }

        protected override void ResetConnector()
        {
            _inSession = false;
            _lastTickInformation = new Dictionary<string, DriverInfo>();
            _lastSessionType = -2;
            _sessionTime = TimeSpan.Zero;
            _sessionStartR3RTime = 0;
            _lastSessionTime = TimeSpan.Zero;
            _lastPlayerMultipleTimesLogged = DateTime.Now;
        }

        public override bool IsConnected => _sharedMemory != null;

        internal TimeSpan SessionTime => _sessionTime;

        protected override string ConnectorName => SimulatorsNameMap.R3ESimName;

        protected override void OnConnection()
        {
            ResetConnector();
            _sharedMemory = MemoryMappedFile.OpenExisting(SharedMemoryName);
        }

        protected override async Task DaemonMethod(CancellationToken cancellationToken)
        {

            while (!ShouldDisconnect)
            {
                await Task.Delay(TickTime, cancellationToken).ConfigureAwait(false);
                R3ESharedData r3RData = Load();
                if (r3RData.GameInReplay == 1)
                {
                    continue;
                }
                SimulatorDataSet data = DataConverter.FromR3EData(r3RData);
                if (CheckSessionStarted(r3RData, data))
                {
                    _lastTickInformation.Clear();
                    _sessionTime = TimeSpan.Zero;
                    _lastSessionTime = _sessionTime;
                    RaiseSessionStartedEvent(data);
                }

                if (r3RData.GamePaused != 1 && r3RData.ControlType != (int) Constant.Control.Replay)
                {
                    if (r3RData.Player.GameSimulationTime != 0)
                    {
                        _sessionTime = TimeSpan.FromSeconds(r3RData.Player.GameSimulationTime - _sessionStartR3RTime);
                    }
                    else
                    {
                        _sessionTime = DateTime.UtcNow - _startSessionTime;
                    }
                }

                _lastSessionTime = data.SessionInfo.SessionTime;
                //R3E sometimes reports the player twice, happens at race finish
                if (data.DriversInfo.Count(x => x.DriverSessionId == data.PlayerInfo?.DriverSessionId) > 1)
                {
                    if ((DateTime.Now - _lastPlayerMultipleTimesLogged).TotalSeconds > 60)
                    {
                        Logger.Info("Player is present more than once in data");
                        _lastPlayerMultipleTimesLogged = DateTime.Now;
                    }

                    continue;
                }
                RaiseDataLoadedEvent(data);

                if (r3RData.ControlType == -1 && !IsProcessRunning())
                {
                    ShouldDisconnect = true;
                }
            }

            _sharedMemory = null;
            _sessionTime = TimeSpan.Zero;
            RaiseDisconnectedEvent();
        }

        internal void StoreLastTickInfo(DriverInfo driverInfo)
        {
            _lastTickInformation[driverInfo.DriverSessionId] = driverInfo;
        }

        private R3ESharedData Load()
        {
            if (!IsConnected)
            {
                throw new InvalidOperationException("Not connected");
            }

            MemoryMappedViewStream view = _sharedMemory.CreateViewStream();
            BinaryReader stream = new BinaryReader(view);
            byte[] buffer = stream.ReadBytes(Marshal.SizeOf(typeof(R3ESharedData)));
            GCHandle handle = GCHandle.Alloc(buffer, GCHandleType.Pinned);
            R3ESharedData data = (R3ESharedData)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(R3ESharedData));
            handle.Free();
            return data;
        }


        private bool CheckSessionStarted(R3ESharedData r3RData, SimulatorDataSet data)
        {
            if (r3RData.SessionType != _lastSessionType)
            {
                Logger.Info("New Session because of session type change");
                _lastSessionType = r3RData.SessionType;
                _sessionStartR3RTime = r3RData.Player.GameSimulationTime;
                _startSessionTime = DateTime.UtcNow;
                return true;
            }

            if (r3RData.SessionPhase != -1 && !_inSession)
            {
                Logger.Info("New session because of session phase change to valid");
                _inSession = true;
                _sessionStartR3RTime = r3RData.Player.GameSimulationTime;
                _startSessionTime = DateTime.UtcNow;
                return true;
            }

            if (_inSession && r3RData.SessionPhase == -1)
            {
                Logger.Info("New session because of session phase change to invalid");
                _sessionStartR3RTime = r3RData.Player.GameSimulationTime;
                _startSessionTime = DateTime.UtcNow;
                _inSession = false;
            }

            if (_lastSessionPhase >= 5 && r3RData.SessionPhase < 5 )
            {
                Logger.Info("New session because of session phase restart");
                _sessionStartR3RTime = r3RData.Player.GameSimulationTime;
                _startSessionTime = DateTime.UtcNow;
                _lastSessionPhase = r3RData.SessionPhase;
                return true;
            }

            if (Math.Abs((data.SessionInfo.SessionTime - _lastSessionTime).TotalSeconds) > 10)
            {
                Logger.Info($"New session because of session time reset OldSession time: {_lastSessionTime.TotalSeconds}, new sessionTime {data.SessionInfo.SessionTime.TotalSeconds}, gamesimulationtime: {r3RData.Player.GameSimulationTime}");
                _sessionStartR3RTime = r3RData.Player.GameSimulationTime;
                _startSessionTime = DateTime.UtcNow;
                return true;
            }

            _lastSessionPhase = r3RData.SessionPhase;
            return false;
        }
    }
}