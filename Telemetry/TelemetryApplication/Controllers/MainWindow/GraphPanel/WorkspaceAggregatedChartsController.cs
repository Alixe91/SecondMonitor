﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.MainWindow.GraphPanel
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AggregatedCharts;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Layouts;
    using SecondMonitor.ViewModels.Layouts.Editor;
    using SecondMonitor.ViewModels.Layouts.Factory;
    using Settings;
    using Synchronization;
    using TelemetryApplication.Settings.DTO;
    using ViewModels.GraphPanel;
    using ViewModels.LoadedLapCache;

    public class WorkspaceAggregatedChartsController : IWorkspaceAggregatedChartsController
    {
        private class ChartWithProvider
        {
            public ChartWithProvider(string providerName, ViewModelContainer wrapperViewModel)
            {
                ProviderName = providerName;
                WrapperViewModel = wrapperViewModel;
            }

            public string ProviderName { get; }
            public ViewModelContainer WrapperViewModel { get; }
        }

        private readonly ITelemetryViewsSynchronization _telemetryViewsSynchronization;
        private readonly ISettingsController _telemetrySettings;
        private readonly AggregatedChartProviderFactory _aggregatedChartProviderFactory;
        private readonly ILoadedLapsCache _loadedLapsCache;
        private readonly Dictionary<string, IAggregatedChartProvider> _providersCache;
        private readonly List<ChartWithProvider> _createdCharts;

        public WorkspaceAggregatedChartsController(ITelemetryViewsSynchronization telemetryViewsSynchronization, ISettingsController telemetrySettings, AggregatedChartProviderFactory aggregatedChartProviderFactory, ILoadedLapsCache loadedLapsCache )
        {
            _providersCache = new Dictionary<string, IAggregatedChartProvider>();
            _createdCharts = new List<ChartWithProvider>();
            _telemetryViewsSynchronization = telemetryViewsSynchronization;
            _telemetrySettings = telemetrySettings;
            _aggregatedChartProviderFactory = aggregatedChartProviderFactory;
            _loadedLapsCache = loadedLapsCache;
        }
        public Task StartControllerAsync()
        {
            _telemetryViewsSynchronization.LapLoaded += TelemetryViewsSynchronizationOnLapLoaded;
            _telemetryViewsSynchronization.LapUnloaded += TelemetryViewsSynchronizationOnLapUnloaded;
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            _telemetryViewsSynchronization.LapLoaded -= TelemetryViewsSynchronizationOnLapLoaded;
            _telemetryViewsSynchronization.LapUnloaded -= TelemetryViewsSynchronizationOnLapUnloaded;
            return Task.CompletedTask;
        }

        public IViewModel GetChartByName(string name)
        {
            IViewModel contentViewModel = GetAggregatedChartByName(name);
            ViewModelContainer wrapper = new ViewModelContainer(contentViewModel);
            _createdCharts.Add(new ChartWithProvider(name, wrapper));
            return wrapper;
        }

        private IViewModel GetAggregatedChartByName(string name)
        {
            IViewModel contentViewModel;
            if (!TryGetProviderForChart(name, out IAggregatedChartProvider chartProvider))
            {
                contentViewModel = new EmptyViewModel();
            }
            else
            {
                contentViewModel = chartProvider.CreateAggregatedChartViewModels(GetChartSettings()).FirstOrDefault();
            }

            return contentViewModel;
        }

        private AggregatedChartSettingsDto GetChartSettings()
        {
            StintRenderingKind preferredChartsKind = _telemetrySettings.TelemetrySettings.AggregatedChartSettings.StintRenderingKind;
            int firstLapStint = _loadedLapsCache.LoadedLaps.FirstOrDefault()?.LapSummary.Stint ?? 0;
            if (preferredChartsKind == StintRenderingKind.None || _loadedLapsCache.LoadedLaps.All(x => x.LapSummary.Stint == firstLapStint))
            {
                return new AggregatedChartSettingsDto()
                {
                    StintRenderingKind = StintRenderingKind.None
                };
            }

            return new AggregatedChartSettingsDto()
            {
                StintRenderingKind = StintRenderingKind.SingleChart
            };

        }

        private bool TryGetProviderForChart(string chartName, out IAggregatedChartProvider aggregatedChartProvider)
        {
            if (_providersCache.TryGetValue(chartName, out aggregatedChartProvider))
            {
                return true;
            }

            aggregatedChartProvider = _aggregatedChartProviderFactory.Create(chartName);
            if (aggregatedChartProvider != null)
            {
                _providersCache[chartName] = aggregatedChartProvider;
            }
            return aggregatedChartProvider != null;
        }

        private void TelemetryViewsSynchronizationOnLapUnloaded(object sender, LapSummaryArgs e)
        {
            RefreshAllAggregatedCharts();
        }

        private void TelemetryViewsSynchronizationOnLapLoaded(object sender, LapTelemetryArgs e)
        {
            RefreshAllAggregatedCharts();
        }

        private void RefreshAllAggregatedCharts()
        {
            foreach (var createdChart in _createdCharts)
            {
                createdChart.WrapperViewModel.ViewModel = GetAggregatedChartByName(createdChart.ProviderName);
            }
        }
    }
}