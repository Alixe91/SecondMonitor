﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Controllers.MainWindow.GraphPanel
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using SecondMonitor.ViewModels;
    using SecondMonitor.ViewModels.Settings;
    using Settings;
    using Synchronization;
    using Synchronization.Graphs;
    using TelemetryApplication.Settings.DTO;
    using TelemetryManagement.DTO;
    using ViewModels.GraphPanel;

    public class WorkspacesSeriesChartsController : IWorkspacesSeriesChartsController
    {
        private readonly ITelemetryViewsSynchronization _telemetryViewsSynchronization;
        private readonly ILapColorSynchronization _lapColorSynchronization;
        private readonly ISettingsProvider _settingsProvider;
        private readonly IGraphViewSynchronization _graphViewSynchronization;
        private readonly ISettingsController _telemetrySettings;
        private readonly SeriesChartFactory _seriesChartFactory;
        private readonly List<LapTelemetryDto> _loadedLaps;
        private TelemetrySettingsDto _telemetrySettingsDto;
        private readonly List<IGraphViewModel> _charts;

        public WorkspacesSeriesChartsController(ITelemetryViewsSynchronization telemetryViewsSynchronization, ILapColorSynchronization lapColorSynchronization, ISettingsProvider settingsProvider,
            IGraphViewSynchronization graphViewSynchronization, ISettingsController telemetrySettings, SeriesChartFactory seriesChartFactory)
        {
            _charts = new List<IGraphViewModel>();
            _telemetryViewsSynchronization = telemetryViewsSynchronization;
            _lapColorSynchronization = lapColorSynchronization;
            _settingsProvider = settingsProvider;
            _graphViewSynchronization = graphViewSynchronization;
            _telemetrySettings = telemetrySettings;
            _seriesChartFactory = seriesChartFactory;
            _loadedLaps = new List<LapTelemetryDto>();
        }

        public Task StartControllerAsync()
        {
            Subscribe();
            _telemetrySettingsDto = _telemetrySettings.TelemetrySettings;
            return Task.CompletedTask;
        }

        public Task StopControllerAsync()
        {
            Unsubscribe();
            _charts.ForEach(x => x.Dispose());
            return Task.CompletedTask;
        }

        public IViewModel GetChartByName(string name)
        {
            IGraphViewModel newChart = _seriesChartFactory.Create(name);
            if (newChart == null)
            {
                return null;
            }

            InitializeViewModel(newChart);
            _charts.Add(newChart);

            return new ChartWrapperViewModel(newChart);
        }

        private void InitializeViewModel(IGraphViewModel graphViewModel)
        {
            graphViewModel.XAxisKind = _telemetrySettingsDto.XAxisKind;
            graphViewModel.GraphViewSynchronization = _graphViewSynchronization;
            graphViewModel.LapColorSynchronization = _lapColorSynchronization;
            graphViewModel.SuspensionDistanceUnits = _settingsProvider.DisplaySettingsViewModel.DistanceUnitsVerySmall;
            graphViewModel.DistanceUnits = _settingsProvider.DisplaySettingsViewModel.DistanceUnitsSmall;
            graphViewModel.VelocityUnits = _settingsProvider.DisplaySettingsViewModel.VelocityUnits;
            graphViewModel.VelocityUnitsSmall = _settingsProvider.DisplaySettingsViewModel.VelocityUnitsVerySmall;
            graphViewModel.TemperatureUnits = _settingsProvider.DisplaySettingsViewModel.TemperatureUnits;
            graphViewModel.PressureUnits = _settingsProvider.DisplaySettingsViewModel.PressureUnits;
            graphViewModel.AngleUnits = _settingsProvider.DisplaySettingsViewModel.AngleUnits;
            graphViewModel.ForceUnits = _settingsProvider.DisplaySettingsViewModel.ForceUnits;
            _loadedLaps.ForEach(graphViewModel.AddLapTelemetry);
        }

        private void Subscribe()
        {
            _telemetryViewsSynchronization.SyncTelemetryView += TelemetryViewsSynchronizationOnSyncTelemetryView;
            _telemetryViewsSynchronization.LapLoaded += TelemetryViewsSynchronizationOnLapLoaded;
            _telemetryViewsSynchronization.LapUnloaded += TelemetryViewsSynchronizationOnLapUnloaded;
        }

        private void Unsubscribe()
        {
            _telemetryViewsSynchronization.SyncTelemetryView -= TelemetryViewsSynchronizationOnSyncTelemetryView;
            _telemetryViewsSynchronization.LapLoaded -= TelemetryViewsSynchronizationOnLapLoaded;
            _telemetryViewsSynchronization.LapUnloaded -= TelemetryViewsSynchronizationOnLapUnloaded;
        }

        private void TelemetryViewsSynchronizationOnSyncTelemetryView(object sender, TelemetrySnapshotArgs e)
        {
            if (e.LapSummaryDto == null)
            {
                return;
            }

            _charts.ForEach(x => x.UpdateXSelection(e.LapSummaryDto, e.TelemetrySnapshot));
        }

        private void TelemetryViewsSynchronizationOnLapUnloaded(object sender, LapSummaryArgs e)
        {
            _loadedLaps.RemoveAll(x => x.LapSummary.Id == e.LapSummary.Id);
            _charts.ForEach(x => x.RemoveLapTelemetry(e.LapSummary));
        }
        private void TelemetryViewsSynchronizationOnLapLoaded(object sender, LapTelemetryArgs e)
        {
            _loadedLaps.Add(e.LapTelemetry);
            _charts.ForEach(x => x.AddLapTelemetry(e.LapTelemetry));
        }
    }
}