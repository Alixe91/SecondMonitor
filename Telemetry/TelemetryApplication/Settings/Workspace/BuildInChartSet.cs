﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Settings.Workspace
{
    public class BuildInChartSet : ChartSet
    {
        public override bool BuildIn => true;
    }
}