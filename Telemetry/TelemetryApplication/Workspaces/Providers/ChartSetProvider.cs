﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Workspaces.Providers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Settings.Workspace;

    public class ChartSetProvider : IChartSetProvider
    {
        private readonly List<IBuildInChartSetProvider> _buildInChartSets;

        public ChartSetProvider(List<IBuildInChartSetProvider> buildInChartSets)
        {
            _buildInChartSets = buildInChartSets;
        }
        public bool TryGetChartSet(Guid chartSetGuid, out ChartSet chartSet)
        {
            var buildInChartSet = _buildInChartSets.FirstOrDefault(x => x.ChartSetGuid == chartSetGuid);
            if (buildInChartSet == null)
            {
                chartSet = null;
                return false;
            }

            chartSet = buildInChartSet.GetChartSet();
            return true;
        }
    }
}