﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Workspaces.Providers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Settings.Workspace;

    public class WorkspaceProvider : IWorkspaceProvider
    {
        private readonly List<IBuildInWorkspaceProvider> _buildInWorkspaces;

        public WorkspaceProvider(List<IBuildInWorkspaceProvider> buildInWorkspaces)
        {
            _buildInWorkspaces = buildInWorkspaces;
        }
        public bool TryGetWorkspace(Guid workspaceGuid, out Workspace workspace)
        {
            var buildInProvider = _buildInWorkspaces.FirstOrDefault(x => x.WorkspaceGuid == workspaceGuid);
            if (buildInProvider == null)
            {
                workspace = null;
                return false;
            }

            workspace = buildInProvider.GetWorkspace();
            return true;
        }
    }
}