﻿namespace SecondMonitor.Telemetry.TelemetryApplication.Workspaces.Providers
{
    using System;
    using Settings.Workspace;

    public interface IWorkspaceProvider
    {
        bool TryGetWorkspace(Guid workspaceGuid, out Workspace workspace);
    }
}