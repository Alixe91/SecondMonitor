﻿namespace SecondMonitor.Telemetry.TelemetryApplication.AggregatedCharts.ScatterPlot.Extractors
{
    using System.Collections.Generic;
    using System.Linq;
    using DataModel.Extensions;
    using DataModel.Telemetry;
    using Filter;
    using OxyPlot;
    using SecondMonitor.ViewModels.Settings;
    using TelemetryManagement.DTO;

    public abstract class AbstractScatterPlotExtractor : AbstractTelemetryDataExtractor
    {
        private readonly IList<ITelemetryFilter> _filters;

        protected AbstractScatterPlotExtractor(ISettingsProvider settingsProvider):this(settingsProvider, new List<ITelemetryFilter>())
        {
        }

        protected AbstractScatterPlotExtractor(ISettingsProvider settingsProvider, IList<ITelemetryFilter> filters) : base(settingsProvider)
        {
            _filters = filters;
        }

        public abstract string YUnit { get; }
        public abstract string XUnit { get; }
        public abstract double XMajorTickSize { get; }
        public abstract double YMajorTickSize { get; }

        public ScatterPlotSeries ExtractSeries(IEnumerable<LapTelemetryDto> loadedLaps, string seriesTitle, OxyColor color)
        {
            return ExtractSeries(loadedLaps, Enumerable.Empty<ITelemetryFilter>(), seriesTitle, color);
        }

        public ScatterPlotSeries ExtractSeries(IEnumerable<LapTelemetryDto> loadedLaps, IEnumerable<ITelemetryFilter> filters, string seriesTitle, OxyColor color)
        {
            List<ITelemetryFilter> allFilters = filters.Concat(_filters).ToList();
            TimedTelemetrySnapshot[] timedTelemetrySnapshots = loadedLaps.SelectMany(x => x.DataPoints).Where(x => allFilters.All(y => y.Accepts(x))).ToArray();

            if (timedTelemetrySnapshots.Length == 0)
            {
                return null;
            }

            ScatterPlotSeries newSeries = new ScatterPlotSeries(color, seriesTitle);
            timedTelemetrySnapshots.ForEach(x => newSeries.AddDataPoint(GetXValue(x), GetYValue(x), x));
            return newSeries;
        }

        protected abstract double GetXValue(TimedTelemetrySnapshot snapshot);
        protected abstract double GetYValue(TimedTelemetrySnapshot snapshot);

    }
}