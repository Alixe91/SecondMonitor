﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels
{
    using System.Collections.Generic;
    using System.Windows.Input;
    using GraphPanel;
    using LapPicker;
    using MapView;
    using SecondMonitor.ViewModels;
    using SnapshotSection;
    using Workspace;

    public interface IMainWindowViewModel : IViewModel
    {
        bool IsBusy { get; set; }
        ILapSelectionViewModel LapSelectionViewModel { get; }
        ISnapshotSectionViewModel SnapshotSectionViewModel { get; }
        IMapViewViewModel MapViewViewModel { get; }

        WorkspaceViewModel WorkspaceViewModel { get; }

        bool IsLapPickerVisible { get; set; }

        ICommand CollapseLapPickerCommand { get; set; }
    }
}