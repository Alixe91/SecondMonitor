﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel.Chassis
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DataModel.BasicProperties;
    using DataModel.Snapshot.Systems;
    using OxyPlot;
    using OxyPlot.Annotations;
    using TelemetryManagement.DTO;

    public class FrontRearBrakesTemperatureChart : AbstractChassisGraphViewModel
    {
        private LineAnnotation _hotTempAnnotation;
        private LineAnnotation _coldTempAnnotation;

        public override string Title => "Front / Rear Brakes Temperatures";
        protected override string YUnits => Temperature.GetUnitSymbol(TemperatureUnits);
        protected override double YTickInterval => 100;
        protected override Func<CarInfo, double> FrontFunc => (x) => Temperature.FromCelsius((x.WheelsInfo.FrontLeft.BrakeTemperature.ActualQuantity.InCelsius + x.WheelsInfo.FrontRight.BrakeTemperature.ActualQuantity.InCelsius) / 2).GetValueInUnits(TemperatureUnits);
        protected override Func<CarInfo, double> RearFunc  => (x) => Temperature.FromCelsius((x.WheelsInfo.RearLeft.BrakeTemperature.ActualQuantity.InCelsius + x.WheelsInfo.RearRight.BrakeTemperature.ActualQuantity.InCelsius) / 2).GetValueInUnits(TemperatureUnits);

        protected override void PreviewOnLapLoaded(LapTelemetryDto lapTelemetryDto)
        {
            base.PreviewOnLapLoaded(lapTelemetryDto);
            var telemetrySnapshot = lapTelemetryDto.DataPoints.FirstOrDefault(x => x.PlayerData != null && !x.PlayerData.CarInfo.WheelsInfo.FrontLeft.BrakeTemperature.IdealQuantity.IsZero && !x.PlayerData.CarInfo.WheelsInfo.FrontLeft.BrakeTemperature.IdealQuantityWindow.IsZero);
            if (telemetrySnapshot == null)
            {
                RemoveLineAnnotations();
                return;
            }
            AddLineAnnotations();
            OptimalQuantity<Temperature> brakeTemperature = telemetrySnapshot.PlayerData.CarInfo.WheelsInfo.FrontLeft.BrakeTemperature;
            _hotTempAnnotation.Y = Temperature.FromCelsius(brakeTemperature.IdealQuantity.InCelsius + brakeTemperature.IdealQuantityWindow.InCelsius).GetValueInUnits(TemperatureUnits);
            _coldTempAnnotation.Y = Temperature.FromCelsius(brakeTemperature.IdealQuantity.InCelsius - brakeTemperature.IdealQuantityWindow.InCelsius).GetValueInUnits(TemperatureUnits);
        }

        private void AddLineAnnotations()
        {
            if (_hotTempAnnotation != null && _coldTempAnnotation != null)
            {
                return;
            }
            _hotTempAnnotation = new LineAnnotation() { Color =  OxyColor.Parse("#ff0000"), StrokeThickness = 2, Type = LineAnnotationType.Horizontal, LineStyle = LineStyle.Solid};
            _coldTempAnnotation = new LineAnnotation() { Color = OxyColor.Parse("#668cff"), StrokeThickness = 2, Type = LineAnnotationType.Horizontal, LineStyle = LineStyle.Solid};
            AddLineAnnotations(new List<Annotation>(){_hotTempAnnotation, _coldTempAnnotation});
        }

        private void RemoveLineAnnotations()
        {
            if (_hotTempAnnotation == null || _coldTempAnnotation == null)
            {
                return;
            }
            RemoveLineAnnotations(new List<Annotation>(){_hotTempAnnotation, _coldTempAnnotation});
        }

    }
}