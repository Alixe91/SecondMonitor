﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel.Chassis
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.Snapshot.Systems;

    public class FrontRearTyreLoadsGraphViewModel : AbstractChassisGraphViewModel
    {
        public override string Title => "Tyre Loads (F/R)";
        protected override string YUnits => Force.GetUnitSymbol(ForceUnits);
        protected override double YTickInterval => Force.GetFromNewtons(1000).GetValueInUnits(ForceUnits);
        protected override bool CanYZoom => true;
        protected override Func<CarInfo, double> FrontFunc => (x) => x.WheelsInfo.FrontLeft.TyreLoad.GetValueInUnits(ForceUnits) + x.WheelsInfo.FrontRight.TyreLoad.GetValueInUnits(ForceUnits);
        protected override Func<CarInfo, double> RearFunc => (x) => x.WheelsInfo.RearLeft.TyreLoad.GetValueInUnits(ForceUnits) + x.WheelsInfo.RearRight.TyreLoad.GetValueInUnits(ForceUnits);
    }
}