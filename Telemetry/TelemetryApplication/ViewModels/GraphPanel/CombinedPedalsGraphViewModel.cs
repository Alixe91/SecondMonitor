﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataModel.Telemetry;
    using OxyPlot;
    using OxyPlot.Series;
    using TelemetryManagement.DTO;

    public class CombinedPedalsGraphViewModel : AbstractGraphViewModel
    {
        public override string Title => "Throttle & Brake";
        protected override string YUnits => "%";
        protected override double YTickInterval => 20;
        protected override bool CanYZoom => true;

        protected override List<LineSeries> GetLineSeries(LapSummaryDto lapSummary, List<TimedTelemetrySnapshot> dataPoints, OxyColor color)
        {
            LineSeries[] lineSeries = new LineSeries[2];
            string baseTitle = $"Lap {lapSummary.CustomDisplayName}";


            List<DataPoint> plotDataPoints = dataPoints.Select(x => new DataPoint(GetXValue(x), x.InputInfo?.ThrottlePedalPosition ?? 0)).ToList();
            lineSeries[0] = CreateLineSeries(baseTitle + "Throttle", OxyColors.Green);
            lineSeries[0].Points.AddRange(plotDataPoints);

            plotDataPoints = dataPoints.Select(x => new DataPoint(GetXValue(x), x.InputInfo?.BrakePedalPosition ?? 0)).ToList();
            lineSeries[1] = CreateLineSeries(baseTitle + "Brake", OxyColors.Red);
            lineSeries[1].Points.AddRange(plotDataPoints);

            YMaximum = 100;
            YMinimum = 0;

            return lineSeries.ToList();
        }

        protected override string GetDataInfo(TimedTelemetrySnapshot timedTelemetrySnapshot)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Throttle: {timedTelemetrySnapshot.InputInfo?.ThrottlePedalPosition ?? 0:G4}");
            sb.AppendLine($"Brake: {timedTelemetrySnapshot.InputInfo?.BrakePedalPosition ?? 0:G4}");
            return sb.ToString();
        }


        protected override void ApplyNewLineColor(List<LineSeries> series, OxyColor newColor)
        {
        }
    }
}