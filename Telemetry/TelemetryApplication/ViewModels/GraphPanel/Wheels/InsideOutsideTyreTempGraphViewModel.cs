﻿namespace SecondMonitor.Telemetry.TelemetryApplication.ViewModels.GraphPanel.Wheels
{
    using System;
    using DataModel.BasicProperties;
    using DataModel.Snapshot.Systems;

    public class InsideOutsideTyreTempGraphViewModel : AbstractWheelsGraphViewModel
    {
        public override string Title => "Inside / Outside Tyre Temps";
        protected override string YUnits => Temperature.GetUnitSymbol(TemperatureUnits);
        protected override double YTickInterval => 1;//Math.Round(Temperature.FromCelsius(25).GetValueInUnits(TemperatureUnits));
        protected override bool CanYZoom => true;
        protected override Func<WheelInfo, double> ExtractorFunction => (x) => Math.Round(Math.Abs(x.LeftTyreTemp.ActualQuantity.GetValueInUnits(TemperatureUnits) - x.RightTyreTemp.ActualQuantity.GetValueInUnits(TemperatureUnits)), 2);
    }
}