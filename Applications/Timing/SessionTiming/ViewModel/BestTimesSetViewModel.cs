﻿namespace SecondMonitor.Timing.SessionTiming.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Drivers.ViewModel;
    using NLog;
    using ViewModels;

    public class BestTimesSetViewModel : AbstractViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private string _setName;
        private SectorTiming _bestSector1;
        private SectorTiming _bestSector2;
        private SectorTiming _bestSector3;
        private ILapInfo _bestLap;

        public BestTimesSetViewModel()
        {
            _setName = string.Empty;
        }

        public BestTimesSetViewModel(string setName)
        {
            _setName = setName;
        }

        public string SetName
        {
            get => _setName;
            set => SetProperty(ref _setName, value);
        }

        public SectorTiming BestSector1
        {
            get => _bestSector1;
            set => SetProperty(ref _bestSector1, value);
        }

        public SectorTiming BestSector2
        {
            get => _bestSector2;
            set => SetProperty(ref _bestSector2, value);
        }

        public SectorTiming BestSector3
        {
            get => _bestSector3;
            set => SetProperty(ref _bestSector3, value);
        }

        public ILapInfo BestLap
        {
            get => _bestLap;
            set => SetProperty(ref _bestLap, value);
        }

        public void CheckAndUpdateBestLap(ILapInfo lap)
        {
            if (BestLap == null || (BestLap.LapTime > lap.LapTime && lap.LapTime != TimeSpan.Zero))
            {
                BestLap = lap;
            }
        }

        public void UpdateSectorBest(SectorTiming sectorTiming)
        {
            switch (sectorTiming.SectorNumber)
            {
                case 1:
                    if ((BestSector1 == null || BestSector1 > sectorTiming) && sectorTiming.Duration > TimeSpan.Zero)
                    {
                        Logger.Info($"New Best Sector 1, By Driver {sectorTiming.Lap.Driver.DriverId}. Sector Time: {sectorTiming.Duration.TotalSeconds} ");
                        Logger.Info(BestSector1 == null ? "Previous Sector Time was NULL" : $"Previous sector time {BestSector1.Duration.TotalSeconds}");
                        BestSector1 = sectorTiming;
                    }

                    break;
                case 2:
                    if ((BestSector2 == null || BestSector2 > sectorTiming) && sectorTiming.Duration > TimeSpan.Zero)
                    {
                        Logger.Info($"New Best Sector 2, By Driver {sectorTiming.Lap.Driver.DriverId}. Sector Time: {sectorTiming.Duration.TotalSeconds} ");
                        Logger.Info(BestSector2 == null ? "Previous Sector Time was NULL" : $"Previous sector time {BestSector2.Duration.TotalSeconds}");
                        BestSector2 = sectorTiming;
                    }

                    break;
                case 3:
                    if ((BestSector3 == null || BestSector3 > sectorTiming) && sectorTiming.Duration > TimeSpan.Zero)
                    {
                        Logger.Info($"New Best Sector 3, By Driver {sectorTiming.Lap.Driver.DriverId}. Sector Time: {sectorTiming.Duration.TotalSeconds} ");
                        Logger.Info(BestSector3 == null ? "Previous Sector Time was NULL" : $"Previous sector time {BestSector3.Duration.TotalSeconds}");
                        BestSector3 = sectorTiming;
                    }

                    break;
            }
        }

        public void FindBestLap(IReadOnlyCollection<DriverTiming> eligibleDrivers)
        {
            BestLap = eligibleDrivers.Select(x => x.BestLap).Where(x => x != null && x.Valid && x.Completed && x.LapTime != TimeSpan.Zero).OrderBy(x => x.LapTime).First();
        }

        public void InvalidateBestSectorForLap(ILapInfo lap, IReadOnlyCollection<DriverTiming> driverEligibleForNewBestSector )
        {
            if (BestSector1 == lap.Sector1)
            {
                BestSector1 = FindBestSector(d => d.BestSector1, driverEligibleForNewBestSector);
            }

            if (BestSector2 == lap.Sector2)
            {
                BestSector2 = FindBestSector(d => d.BestSector2, driverEligibleForNewBestSector);
            }

            if (BestSector3 == lap.Sector3)
            {
                BestSector3 = FindBestSector(d => d.BestSector3, driverEligibleForNewBestSector);
            }
        }

        private SectorTiming FindBestSector(Func<DriverTiming, SectorTiming> sectorTimeFunc, IReadOnlyCollection<DriverTiming> driverEligibleForNewBestSector)
        {
            return driverEligibleForNewBestSector.Where(d => d.IsActive).Select(sectorTimeFunc).Where(s => s != null)
                .OrderBy(s => s.Duration).FirstOrDefault();
        }
    }
}