﻿namespace SecondMonitor.Timing.SessionTiming.Drivers.ViewModel
{
    using System;
    using ViewModels;
    using ViewModels.Settings.ViewModel;

    public class LapDeltaViewModel : AbstractViewModel, IDisposable
    {
        public const string ViewModelLayoutName = "Lap Detla";
        private  DriverTiming _driver;
        private DisplaySettingsViewModel _displaySettingsViewModel;
        private LapPortionTimesComparatorViewModel _playerLapToPrevious;
        private LapPortionTimesComparatorViewModel _playerLapToPlayerBest;

        public LapDeltaViewModel()
        {
            PlayerLapToBestPlayerComparator = new LapPortionTimesComparatorViewModel();
            PlayerLapToPreviousComparator = new LapPortionTimesComparatorViewModel();
            RecreatePlayerLapToPlayerBest();
        }

        public DriverTiming Driver
        {
            get => _driver;
            set
            {
                UnSubscribeToDriverEvents();
                _driver = value;
                SubscribeToDriverEvents();
            }
        }

        private void SubscribeToDriverEvents()
        {
            if (_driver == null)
            {
                return;
            }

            _driver.NewLapStarted += DriverOnLapCompletedOrInvalidated;
            _driver.LapCompleted += DriverOnLapCompletedOrInvalidated;
            _driver.LapInvalidated += DriverOnLapCompletedOrInvalidated;
        }

        private void UnSubscribeToDriverEvents()
        {
            if (_driver == null)
            {
                return;
            }

            _driver.NewLapStarted -= DriverOnLapCompletedOrInvalidated;
            _driver.LapCompleted -= DriverOnLapCompletedOrInvalidated;
            _driver.LapInvalidated -= DriverOnLapCompletedOrInvalidated;
        }

        private void DriverOnLapCompletedOrInvalidated(object sender, LapEventArgs e)
        {
            RecreatePlayerLapToPlayerBest();
            RecreatePlayerLapToPrevious();
        }

        public LapPortionTimesComparatorViewModel PlayerLapToPreviousComparator
        {
            get => _playerLapToPrevious;
            protected set => SetProperty(ref _playerLapToPrevious, value);
        }

        public LapPortionTimesComparatorViewModel PlayerLapToBestPlayerComparator
        {
            get => _playerLapToPlayerBest;
            protected set => SetProperty(ref _playerLapToPlayerBest, value);
        }

        public DisplaySettingsViewModel DisplaySettingsViewModel
        {
            get => _displaySettingsViewModel;
            set => SetProperty(ref _displaySettingsViewModel, value);
        }


        private void RecreatePlayerLapToPrevious()
        {
            _playerLapToPrevious?.Dispose();

            if (_driver.LastCompletedLap == null || _driver.CurrentLap == null)
            {
                return;
            }

            PlayerLapToPreviousComparator.ChangeReferencedLaps(_driver.LastCompletedLap, _driver.CurrentLap);
        }

        private void RecreatePlayerLapToPlayerBest()
        {
            if (_driver == null)
            {
                return;
            }
            _playerLapToPlayerBest?.Dispose();

            if (_driver.BestLap == null || _driver.CurrentLap == null)
            {
                return;
            }

            PlayerLapToBestPlayerComparator.ChangeReferencedLaps(_driver.BestLap, _driver.CurrentLap);
        }

        public void Dispose()
        {
            _playerLapToPrevious?.Dispose();
            _playerLapToPlayerBest?.Dispose();
            _driver.NewLapStarted -= DriverOnLapCompletedOrInvalidated;
            _driver.LapCompleted -= DriverOnLapCompletedOrInvalidated;
            _driver.LapInvalidated -= DriverOnLapCompletedOrInvalidated;
        }
    }
}